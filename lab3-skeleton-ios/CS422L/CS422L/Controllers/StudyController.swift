//
//  StudyController.swift
//  CS422L
//
//  Created by Student Account  on 2/7/22.
//

import UIKit

class StudyController: UIViewController {
    
    var flashCardList = Flashcard.getHardCodedFlashcards()
     var missed_items = Array<Flashcard>()
    var num_missed = 0
    var num_correct = 0
    var num_comleted = 0
    var isTerm = true
    
   
    @IBOutlet weak var correct_label: UILabel!
    
    @IBOutlet weak var missed_label: UILabel!
    
    @IBOutlet weak var totalComplete: UILabel!
    
    @IBOutlet weak var term: UILabel!
    
     
    override func viewDidLoad() {
          super.viewDidLoad()
        term.text = flashCardList[0].term
      }
    
    
    
    
        func isInList(list: Array<Flashcard>, card: Flashcard)-> Bool {
        for flashCard in list{
            if (card === flashCard){
                return true
            }
        }
        return false
    }
    
     
    @IBAction func OnMiss(_ sender: Any) {
        num_missed+=1
        var missed_Card = flashCardList[0]
        flashCardList.remove(at: 0)
        flashCardList.append(missed_Card)
        missed_items.append(missed_Card)
        missed_label.text = String(num_missed)
        // make label of missed show missed variable.
        term.text = flashCardList[0].term
    }
    
    
    
    
    @IBAction func OnSkip(_ sender: Any) {
        var skipped_card = flashCardList[0]
        flashCardList.remove(at: 0)
        flashCardList.append(skipped_card)
        term.text = flashCardList[0].term
        // make the term label show new flashcardlist[0]
        term.text = flashCardList[0].term
    }
    
    
    
    
    @IBAction func OnCorrect(_ sender: Any) {
        var correct_card = flashCardList[0]
        num_comleted+=1
        totalComplete.text = String(num_comleted)
        if (isInList(list:missed_items,card:correct_card) == false) {
            num_correct+=1
            correct_label.text = String(num_correct)
            // update correct label
        }
       
        // update completed label
        flashCardList.remove(at: 0)
        term.text = flashCardList[0].term
      
    }
    

}
