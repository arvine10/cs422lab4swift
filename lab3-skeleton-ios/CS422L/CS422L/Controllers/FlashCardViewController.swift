//
//  FlashCardViewController.swift
//  CS422L
//
//  Created by Student Account  on 08/11/1400 AP.
//

import Foundation
import UIKit

class FlashCardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var sets: [Flashcard] = [Flashcard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let setClass = FlashcardSet()
        //connect hard coded collection to sets
        // drag in an outlet for collection view 
        // collectionviewdelagate = self
        // collectionview.datasource = se;f
        tableView.delegate = self
        tableView.dataSource = self
        sets = Flashcard.getHardCodedFlashcards()
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sets.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "SetCell2", for: indexPath) as! FlashcardCollectionCell
        //setup cell display here
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        cell.addGestureRecognizer(longPressRecognizer)
        
        cell.fclabel.text = sets[indexPath.row].term
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: sets[indexPath.row].term, message: sets[indexPath.row].definition, preferredStyle: UIAlertController.Style.alert)
        
        let OKAction = UIAlertAction(title: "edit", style: .default) { (action:UIAlertAction!) in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertController")as! UIViewController
                self.present(vc,animated: true)
            }
        alert.addAction(OKAction)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    
    
    
    
//        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "SetCell2", for: indexPath) as! FlashcardCollectionCell
//
//            let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: "longPressed")
//            cell.addGestureRecognizer(longPressRecognizer)
//            return cell
//        }

    
    @objc func longPressed(sender: UITapGestureRecognizer){
    
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertController") as! UIViewController
            self.present(vc,animated: true)
        
        
    }     
    
      
    
    @IBAction func addClicked(_ sender: Any) {
        var newflash = Flashcard()
        newflash.term = "new Term"
        newflash.definition = "new defeintion"
        sets.append(newflash)
        tableView.reloadData()
    }
    
    
    
    @IBAction func onStudy(_ sender: Any) {
        performSegue(withIdentifier: "GoToStudy", sender: self)
       
    }
    
    
    
    
    
    
    
    
}
